function hideResults() {
    document.getElementById("startingAmount").value = (0);
    document.getElementById("currentAmount").value = (0);
    document.getElementById("highestAmount").value = (0);
    document.getElementById("totalRollCount").value = (0);
    document.getElementById("rollsAtHighestAmount").value = (0);
    document.getElementById("resultsShown").style.display = "none";
}
function getRandomDice() {
    var diceTotal;
    var startingAmount = parseInt(document.getElementById("startingAmount").value);
    var currentAmount = startingAmount;
    var amount = [];
    amount.push(currentAmount);
    while (currentAmount > 0) {
        diceTotal = Math.floor((Math.random() * 6) + 1) + Math.floor((Math.random() * 6) + 1);
        if (diceTotal == 7)
        {
            currentAmount = currentAmount + 4;
        } else {
            currentAmount = currentAmount - 1;
        }
        amount.push(currentAmount);
    }
    var totalRollCount = amount.length - 1;
    var highestAmount = Math.max.apply(Math, amount);
    var rollsAtHighestAmount = amount.indexOf(highestAmount);
    document.getElementById("startingAmount1").value = startingAmount;
    document.getElementById("totalRollCount").value = totalRollCount;
    document.getElementById("highestAmount").value = highestAmount;
    document.getElementById("rollsAtHighestAmount").value = rollsAtHighestAmount;
    document.getElementById("startingAmount").value = null;
    document.getElementById("startingAmount").focus();
    document.getElementById("resultsShown").style.display = "show";

}
