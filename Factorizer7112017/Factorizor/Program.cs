﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exitProgram = false;
            while (!exitProgram)
            {
                int number = GetNumberFromUser();

                Calculator.PrintFactors(number);
                Calculator.IsPerfectNumber(number);
                Calculator.IsPrimeNumber(number);
                Console.WriteLine("Would you like to factor another number? Y or N");
                string response = Console.ReadLine().Trim();
                if (response.Equals("n")||(response.Equals("N")))
                {
                    exitProgram = true;
                }
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        /// <summary>
        /// Prompt the user for an integer.  Make sure they enter a valid integer!
        /// 
        /// See the String Input lesson for TryParse() examples
        /// </summary>
        /// <returns>the user input as an integer</returns>
        static int GetNumberFromUser()
        {
            string num = "";
            int number;
            while (!int.TryParse(num, out number) || number < 1)
            {
                Console.WriteLine("What number would you like to factor?");
                num = Console.ReadLine();
                if (!int.TryParse(num, out number) || number < 1)
                {
                    Console.WriteLine("Must be a NUMBER greater than 0!!");

                }

            }
            return number;
        }
    }

    class Calculator
    {
        /// <summary>
        /// Given a number, print the factors per the specification
        /// </summary>
        public static void PrintFactors(int number)
        {
            Console.WriteLine("The factors of " + number + " are: " + GetFactors(number));
        }
        public static string GetFactors(int number)
        {
            string factorString = "";
            for (int i = 1; i <= number; i++)
            {
                if (number % i == 0)
                {
                    factorString += i + " ";
                }
            }
            return factorString;

        }

        /// <summary>
        /// Given a number, print if it is perfect or not
        /// </summary>
        public static void IsPerfectNumber(int number)
        {
            int factorTotal = 0;
            String factors = GetFactors(number).Trim();
            String[] factorsOfN = factors.Split(' ');
            for (int i = 0; i <= factorsOfN.Length - 2; i++)
            {
                factorTotal += int.Parse(factorsOfN[i]);
            }
            if (factorTotal == number)
            {
                Console.WriteLine(number + " is a perfect number.");

            }
            else
                Console.WriteLine(number + " is NOT a perfect number.");
        }

        public static void IsPrimeNumber(int number)
        {
            String factors = GetFactors(number).Trim();
            String[] factorsOfN = factors.Split(' ');
            if (factorsOfN.Length == 2)
            {
                Console.WriteLine(number + " is a prime number.");
            }
            else
                Console.WriteLine(number + " is NOT a prime number.");
        }
    }
}

/// <summary>
/// Given a number, print if it is prime or not
/// </summary>
//public static void IsPrimeNumber(int number)

