function initialize() {
    var yourName = document.getElementById("yourName").value = null;
    var phone = document.getElementById("phone").value = null;
    var email = document.getElementById("email").value = null;
    var additionalInformation = document.getElementById("additionalInformation").value = null;
}
function isEmpty() {
    var yourName = document.getElementById("yourName").value;
    var phone = document.getElementById("phone").value;
    var email = document.getElementById("email").value;
    var reason = document.getElementById("reason").selectedIndex;
    var additionalInformation = document.getElementById("additionalInformation").value;
    if (yourName == "") {
        alert("Please enter your name");
    }
    if (phone == "" && email == "") {
        alert("Please enter a method of contact");
    }
    if (reason == "Other" && additionalInformation.length === 0) {
        alert("Please enter additional information");
    }
    if (document.getElementById("Monday").unchecked &&
            document.getElementById("Tuesday").unchecked &&
            document.getElementById("Wednesday").unchecked &&
            document.getElementById("Thursday").unchecked &&
            document.getElementById("Friday").unchecked
            ) {
        alert("What days are you available to contact?");
        return false;
    } else {
        return true;
    }
}
